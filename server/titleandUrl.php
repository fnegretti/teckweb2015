<?php


include_once('simple_html_dom.php');


$count=0;
$arr=array();
$json = @file_get_contents('../example_collection.json');
$json_a = json_decode($json, true);
foreach($json_a as $item) {
    for($i=0; $i <sizeof($item); $i++){
        $array_dei_titli = $item[$i]["Title"];
        for($j=0;$j<sizeof($array_dei_titli);$j++){
            $titleDocument = $array_dei_titli[$j];
            $html = @file_get_html('./documenti/D-Lib/'.$titleDocument.'.html');
            if ($html == FALSE){
                $html = @file_get_html('./documenti/Statistica/'.$titleDocument.'.html');
                if($html == FALSE){
                    $html = @file_get_html('./documenti/D-Lib Issue/'.$titleDocument.'.html');
                    if($html == FALSE){
                        $html = @file_get_html('./documenti/AlmaDL Journals/'.$titleDocument.'.html');
                        $type = 'AlmaDL Journals';
                    }else{
                        $type = 'D-Lib Issue';
                    }
                }else{
                    $type = 'Statistica';
                }
            }else{
                $type = 'D-Lib';
            }
            $count++;
            $URL = openDocument($html,$type);
            $elemento = array('Title' => trim($titleDocument), 'Url'=>trim($URL));
            array_push($arr,$elemento);
        }
    }
}

print_r($arr);
file_put_contents('TitleToUrl.json', json_encode($arr));

function openDocument($html,$type){
    $dom = new DOMDocument;
    @$dom->loadHTML($html);
    $xpath = new DOMXPath($dom);
    if($type!="Statistica" && $type!="AlmaDL Journals"){
        // predo l url
        $nodeUrl = $xpath->query("//a[@class='fc']")->item(0);
        $URL= $nodeUrl->getAttribute('href');
    }else{
        $nodeUrl = $xpath->query("//meta[@name='citation_abstract_html_url']")->item(0);
        $URL= $nodeUrl->getAttribute('content');
    }
    return $URL;
}
?>