<?php
	include_once('simple_html_dom.php');
	$titleDocument =  $_GET["title"];
	$type = $_GET["type"];
	$arrAuthor["Author"] = array();
	$html = file_get_html('./documenti/'.$type.'/'.$titleDocument.'.html');
	$dom = new DOMDocument;
	@$dom->loadHTML($html);
	$xpath = new DOMXPath($dom);
	//Controllo type della rivista
	if($type!="Statistica" && $type!="AlmaDL Journals"){
		
		//prendo DOI
		$node = $xpath->query("//meta[@name='DOI']")->item(0);
		$DOI =$node->getAttribute('content');

		//prendo gli autori
		$node = $xpath->query("//p[@class='blue']/b");
		foreach ($node as $key => $value) {
			array_push($arrAuthor["Author"],$value->textContent);
		}
		
		//prendo l anno
		$nodes = $xpath->query("//p[@class='blue']")->item(0);
		$node = explode(" ", $nodes->textContent);

		// predo l url
		$nodeUrl = $xpath->query("//a[@class='fc']")->item(0);
		$URL= $nodeUrl->getAttribute('href');
	}else{
		
		//DOI
		$node = $xpath->query("//meta[@name='DC.Identifier.DOI']")->item(0);
		$DOI =$node->getAttribute('content');
		
		//autori
		$node = $xpath->query("//div[@id='authorString']/em");
		foreach ($node as $key => $value) {
			array_push($arrAuthor["Author"],$value->textContent);
		}
		
		//url
		$nodeUrl = $xpath->query("//meta[@name='citation_abstract_html_url']")->item(0);
		$URL= $nodeUrl->getAttribute('content');
		
		//data
		$nodes = $xpath->query("//meta[@name='DC.Date.created']")->item(0);
		$node = explode("-", $nodes->getAttribute('content'));
		$node[1]= $node[0];
	}
	
	$arr = array('DOI' => $DOI, 'TitleDocument' => $titleDocument, 'Year' => $node[1], "URL" => $URL);
	
	$merge = array_merge($arr,$arrAuthor);
	
	echo json_encode($merge);

?>