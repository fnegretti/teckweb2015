<?php 

	
	include_once('simple_html_dom.php');
	$titleDocument =  $_GET["title"];
	

	//caricamento reale del documento e relative immagini rcuperandolo dalla cartella

		
	$html = @file_get_html('./documenti/D-Lib/'.$titleDocument.'.html');
	if ($html == FALSE){
		$html = @file_get_html('./documenti/Statistica/'.$titleDocument.'.html');	
		if($html == FALSE){
			$html = @file_get_html('./documenti/D-Lib Issue/'.$titleDocument.'.html');
			if($html == FALSE){
				$html = @file_get_html('./documenti/AlmaDL Journals/'.$titleDocument.'.html');
				$type = 'AlmaDL Journals';
			}else{
				$type = 'D-Lib Issue';
			}
		}else{
			$type = 'Statistica';
		}
	}else{
		$type = 'D-Lib';
	}

	openDocument($html, $type);


	
	function openDocument($html, $type){
		$dom = new DOMDocument;
		@$dom->loadHTML($html);
		$xpath = new DOMXPath($dom);
		$nodes = $xpath->query("//img");
		//rimpiazzo il persorso negli html delle immagini per caricrle dal nostro percorso
		foreach($nodes as $node) {
			$attributi=explode("/",$node->getAttribute('src'));

	    	$newPath = './server/documenti/'.$type.'/'.$attributi[1].'/'.$attributi[2];
	    	
			$node->setAttribute('src', $newPath);
		}
		$nodes = $xpath->query("//script");
		foreach($nodes as $node){
			 $node->parentNode->removeChild($node);
		}
		$nodes = $xpath->query("//link");
		foreach($nodes as $node){
			 $node->parentNode->removeChild($node);
		}
		$nodes = $xpath->query("//meta");
		foreach($nodes as $node){
			 $node->parentNode->removeChild($node);
		}

		$myJson = array("Documento"=> $dom->saveHTML(), "Type"=> $type);

		echo json_encode($myJson);
		

	}
 ?>