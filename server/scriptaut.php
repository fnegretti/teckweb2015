<?php

include_once('simple_html_dom.php');
$type=array('D-Lib','Statistica','D-Lib Issue',"AlmaDL Journals");
$count=0;
$string = file_get_contents("../example_collection.json");
$json_a = json_decode($string, true);
global $arrAuthor,$id;
$arrAuthor = array();
$id=array();
$finalArr = array();
foreach ($json_a["Documenti"] as $key ){


    foreach($key["Title"] as $value ){
        $titleDocument = $value;
        $html = file_get_html('./documenti/'.$type[$count].'/'.$titleDocument.'.html');
        $dom = new DOMDocument;
        @$dom->loadHTML($html);
        $xpath = new DOMXPath($dom);

        if($type[$count]!="Statistica" && $type[$count]!="AlmaDL Journals"){
            //prendo gli autori
            $node = $xpath->query("//p[@class='blue']/b");
            foreach ($node as $key => $aut) {
                $autore=urldecode($aut->textContent);
                $autore=strtolower($autore);
                $autore = preg_replace("/[^a-zA-Z 0-9]+/", " ", $autore);
                $autore=trim($autore);
                //echo $autore." ".$contatoreDlib."\n";
                array_push($arrAuthor,$aut->textContent);
                //print_r(explode(' ', $autore, 2));
                $expl=explode(' ', $autore, 2);
                $expl[1]=str_replace(' ', '', $expl[1]);
                array_push($id,$expl[1]."-".substr($expl[0],0,1));

            };
        }else {
            //autori

            $node = $xpath->query("//div[@id='authorString']/em");

            foreach ($node as $key => $aut) {

                $autore=urldecode($aut->textContent);
                $temp = explode(",",$autore);


                if(sizeof($temp)== 1) {
                    $autore = strtolower($autore);
                    $autore = preg_replace("/[^a-zA-Z 0-9]+/", " ", $autore);
                    $autore = trim($autore);
                    array_push($arrAuthor, trim($aut->textContent));
                    $expl = explode(' ', $autore, 2);
                    $expl[1] = str_replace(' ', '', $expl[1]);
                    array_push($id, $expl[1] . "-" . substr($expl[0], 0, 1));

                }else{
                    for($k=0;$k<sizeof($temp);$k++){
                        $currentAutor= $temp[$k];
                        $temp[$k] = strtolower($temp[$k]);
                        $temp[$k] = preg_replace("/[^a-zA-Z 0-9]+/", " ", $temp[$k]);
                        $temp[$k] = trim($temp[$k]);
                        // echo $autore." ".$contatoreStat."\n";
                        array_push($arrAuthor, trim($currentAutor));
                        $expl = explode(' ', $temp[$k], 2);
                        $expl[1] = str_replace(' ', '', $expl[1]);
                        array_push($id, $expl[1] . "-" . substr($expl[0], 0, 1));


                    }
                }
            };
        }

    }
    $count++;
};


for($i=0;$i<sizeof($arrAuthor);$i++){
    $finalArr[$arrAuthor[$i]]=$id[$i];

};
file_put_contents('authorDictionary.json', json_encode($finalArr));
?>