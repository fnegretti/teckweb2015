var JsonDocInfo;
var globalSelectionObj = {};
var idauto;
var TotAnnotation=[]; // tutti i json annotazioni per un utente
var indexAnn=0;
var JsonAutori = {};
var autoritotali=[];
var indexCorrente=0;
var JsonTitle={};
$( document ).ready(function(){

    var availableTags;
    var myJson;
    var logged = 0;
    var idselect;
    var url = "example_collection.json";

    //caricamento JSON per l'autocomplete
    var availableTags = jQuery.parseJSON(
        jQuery.ajax({
            url: url, 
            async: false,
            dataType: 'json'
        }).responseText
      );
    
    vb = function(){
        myJson = [];
        for( var i = 0 ; i < 4 ; i++){
            myJson = $.merge(availableTags.Documenti[i].Title, myJson);
        };
        return myJson;
    }

    getJsonTitle();

    //riempimento autocomplete e settaggio keypress
    $("#spotlight").autocomplete({
    source: vb(),
    appendTo: $("form:first"),
    select: function( event, ui ) {
        if(event.which == 13)
            loadDocumentFromSever(ui.item.value);
            $('.info-doc').removeClass('nascondi');
            $('.list-doc').addClass('list-doc2');
    }
    });
    $("#spotlight").data( "ui-autocomplete" )._renderMenu = function( ul, items ) {
      var that = this;
      ul.attr("class", "nav nav-pills nav-stacked");
      $.each( items, function( index, item ) {
        that._renderItemData( ul, item );

      });
    };
    $("#spotlight").keypress(function(e){
        if(e.which == 13) {
            loadDocumentFromSever($("#spotlight").val());
            $('.info-doc').removeClass('nascondi');
            $('.list-doc').addClass('list-doc2');
        }
    });

    // finestra di Login
    $("#login").click(function(){
        if($.cookie("username")!= null || $.cookie("mail")!= null){
            if($.cookie("username") != document.login_information.user.value || $.cookie("mail") != document.login_information.mail.value)
                alert("nome o mail errati");
        return;
        }
        else if($.cookie("username" == null) && $.cookie("mail" == null) && (document.login_information.user.value!='') && (document.login_information.mail.value!='')){
            $.cookie("username",document.login_information.user.value);
            $.cookie("mail",document.login_information.mail.value);
            alert("utente creato");
            $.cookie("logged",1);
            $('#login-annotator').text("Reader");
            return;
        }
        else{
            alert("errore login");
            return;
        }
        $.cookie("logged",1);
        $('#login-annotator').text("Reader");
    });

    //aggiunge i title dei documenti nella colonna di sinistra attraverso un json
    loadingDocument(myJson);

    //selezione porzione da evidenziare ed inserisce l'immagine per aprire il modal
    $('div#document').mouseup(function(){
        if($.cookie("logged") !=1) return;
        var selezione = getSelectedText();
        if (window.getSelection) {
            if (window.getSelection().empty) {  // Chrome
                window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) {  // Firefox
                window.getSelection().removeAllRanges();
            }
        } else if (document.selection) {  // IE?
            document.selection.empty();
        }
        if(selezione != null){
            $('.current').after("<img onclick='modalFrammentAnnotation()' href='#' data-testo='"+selezione+"' data-toggle='modal' data-target='#myModalFramment' src='javascript/immagini/annotation.png' class='giacomo'>");
            convert($('.current').getPath());
        }

    });

    //toglie la selezione in caso l utente faccia un "click" ovvero mouse down :P sempre sulla finestra del document ma da una parte non selezionata
    //invece se fa click sull icona che apre il modal la fa
    $('#document').mousedown(function(e){

        var elem = e.target;
        console.log(elem.getAttribute("class"));
        if(elem.getAttribute("class")!= 'giacomo' && e.which != 3  ){
            $(document).find('.current').removeClass("selectedText current");
            $('span').contents().unwrap()
            $(document).find('.giacomo').remove();
        };
    });

    setclick('.nav-stacked >'+'.ui-menu-item');
    setclick('#documenti li');

    //apertura modal per modifica annotazioni su documento
    $('.list-group-item ').on('click', function(){
        if($.cookie("logged") != 1) return;
        $('#myModalDocument').modal('show');
        idselect=($(this).context.getAttribute('id'));
        autoritotali=elencoAutori();
       // console.log(autoritotali);
        modalDocumentAnnotation(idselect);

    });

    //pulsanti cancella e conferma modal annotazione frammenti
    $('#cancelButtonModalFramment').on('click', function(){
        $('.modal-body').empty();
        $('#myModalFramment').modal('toggle');

        $(document).find('.current').removeClass("selectedText current");
        $('span').contents().unwrap()
        $(document).find('.giacomo').remove();
    });

    $('#cancelButtonModalDocument').on('click',function(){
        $('.modal-body').empty();
        $('#myModalDocument').modal('toggle');
    });

    $("#saveButtonModalDocument").on('click', function(){

        var typeAnnotation =  $('.modal-document' + '> div' + '> input').attr('id');
        var label = $('.modal-document' + '> div' + '> input').attr('placeholder');
        var document = $('#HasTitleDoc '+'> i'+'>span').text();
        console.log($('#modaldocuementcheck').length);
        if($('#modaldocuementcheck').length ==1){
            var frammentdocument = $('#modaldocuementcheck').is(':checked') ;
        }else{
            var frammentdocument = 1;
        }
        var Url = JsonDocInfo['URL'];
        var valueAnnotation = $('.modal-document' + '> div' + '> input').val();
        if(typeAnnotation== 'hasAuthor') {
            var exist = $.inArray($("#hasAuthor").val(), autoritotali);
            if (exist == -1) {
                $("#errormessage").removeClass("nascondi");
                $("#hasAuthor").val("");
                return;
            }
        }
        makeaJsonOnDocument(typeAnnotation,label,document,frammentdocument,valueAnnotation,UrlFix(Url));
        $('#myModalDocument').modal('hide');


    });

    $("#UploadAnnotationConfirm").on('click',function(){
        if (TotAnnotation[indexCorrente].annotation[0].type == 'hasAuthor'){
            TotAnnotation[indexCorrente].annotation[0].body.resource.label = $("#newAuthor").val();
            TotAnnotation[indexCorrente].annotation[0].body.resource.id = searchAut($("#newAuthor").val());
            $('#annotationTable>tbody>tr:eq('+indexCorrente+')> .valueAnnotation').text($("#newAuthor").val());
        }else{
            TotAnnotation[indexCorrente].annotation[0].body.literal = $("#newAuthor").val();
            $('#annotationTable>tbody>tr:eq('+indexCorrente+')> .valueAnnotation').text($("#newAuthor").val());
        }
        $('#myModalModify').modal("hide");
    });

    $("#UploadAnnotationCancel").on('click',function(){
        $('#myModalModify').modal("hide");
    });

    $('#openTempAnnModal').on('click',function(){
        $('#tempAnn').modal('show');
        newRow();
    });

    $('#closeModalTemp').on('click',function(){
        $('#tempAnn').modal("hide");
    });

});//chiusura .ready

//questa funzione permette attraverto getPath di prendere la location in termini di tag html dalla nostra selezione
//usage: console.log($(".selectedText").getPath()) result: form_table3_tbody_tr_td_table5_tbody_tr_td_table_tbody_tr_td2_p10_span
//TODO bisogna formattare meglio la stringa perche inserendo > al posto di _ possiamo accedere al testo selezionato ;
jQuery.fn.extend({
    getPath: function () {
        var path, node = this;
        while (node.length) {
            var realNode = node[0], name = realNode.localName;
            if (!name || realNode.getAttribute("id")=="document") break;
            name = name.toLowerCase();

            var parent = node.parent();

            var sameTagSiblings = parent.children(name);
            if (sameTagSiblings.length > 1) {
                allSiblings = parent.children();
                var index = allSiblings.index(realNode) + 1;
                if (index > 1) {
                    name +=  index;
                }
            }

            path = name + (path ? '_' + path : '');
            node = parent;
        }
        path=path.substring(0,path.length-5);
        return path;
    }
});

//aggiunta documenti nella MetaArea laterale dei documenti
function loadingDocument(myJson) {
    $.each(myJson, function( key, value) {
        $("#documenti").append( "<li>" + value + "</li>" );
    });
};

//esegue la chiamata ajax per reperire le informazioni dallo scraper.php
function loadDocumentInfo(titleDocument , type){
 $.ajax({
         // definisco il tipo della chiamata
      type: "GET",
      // specifico la URL della risorsa da contattare
      url: "server/scrapper.php",
      // passo dei dati alla risorsa remota
      data: { "title": titleDocument, "type": type },
      // definisco il formato della risposta
      dataType: "json",
      // imposto un'azione per il caso di successo
      success: function(risposta){

          JsonDocInfo = risposta;
        compileInfoDoc(JsonDocInfo);
      },
      error: function(e){
        console.log(e.responseText);
      }
  })
}

//inserisce le informazioni recuperate dallo scraper all interno di una tabella
function compileInfoDoc(JsonDocInfo){
    var tempUrl = (JsonDocInfo['URL']);
    var Url = UrlFix(tempUrl);
    $('.list-group-item').empty()
    $("#HasAuthorDoc").append('<i> <b>Autori:</b></i>');
    $.each(JsonDocInfo['Author'], function(i, item) {
        $("#HasAuthorDoc i").append(JsonDocInfo['Author'][i]+',');
    });
    $("#HasTitleDoc").append('<i>  <b>Titolo:</b>'+'<span>'+ JsonDocInfo['TitleDocument']+'</span>'+'</i>');
    $("#HasYearDoc").append('<i> <b>Anno:</b>'+ JsonDocInfo['Year']+'</i>');
    $("#URLDoc").append('<i> <b>URL:</b>'+Url+'</i>');
    $("#DOIDoc").append('<i>  <b>DOI:</b>'+ JsonDocInfo['DOI']+'</i>');
}

//riempie il modal con una select dove sono specificate tutte le annotazioni che si possono fare sul documento
function modalFrammentAnnotation(){
    var selectedText = $('.giacomo').data('testo');
    $('.modal-framment').append('<div>' +
        '<label for="selectann">'+selectedText+'</label>'+
        '<select id="selectann" class="form-control">' +
        '<option disabled selected> -- select an option -- </option>'+
        '  <option value="hasComment">Has Comment</option>' +
        '  <option value="denotesRhetoric">Denotes Retoric</option>' +
        '  <option value="cites">Cites</option>' +
        ' </select> </div>');
    $(document).on('change','#selectann', function(){
        var strUser = $(this).val();
        $('.content').remove();
        if(strUser=='hasComment'){
            $('.modal-framment').append('<div class="input-group content"><input type="text" id="commentText" class="form-control" placeholder="Has Comment" aria-describedby="basic-addon1"></div>')
    
        }else if(strUser=="denotesRhetoric"){
            $('.modal-framment').append('<div class="radio content">'+
                '<label><input type="radio" name="optradio">Option 1</label>'+
                '</div>'+
                '<div class="radio content">'+
                '<label><input type="radio" name="optradio">Option 2</label>'+
                '</div>'+
                '<div class="radio content">'+
                '<label><input type="radio" name="optradio">Option 3</label>'+
                '</div>');
        }else if(strUser=="cites"){
            $('.modal-framment').append('<div class="input-group content">'+
                '<input type="text" class="form-control" placeholder="Has Author" aria-describedby="basic-addon1">'+
                '<input type="text" class="form-control" placeholder="Has Title" aria-describedby="basic-addon1">'+
                '<input type="text" class="form-control" placeholder="Pubblication Year" aria-describedby="basic-addon1">'+
                '<input type="text" class="form-control" placeholder="DOI" aria-describedby="basic-addon1">'+
                '<input type="text" class="form-control" placeholder="URI" aria-describedby="basic-addon1">'+ 
                '</div>');
        } 
    });
}    

//riempie il modal con il giusto input per quanto riguarda l annotazione su tutto il documento 
function modalDocumentAnnotation(id){
    $('.modal-document > h4').remove();
    var strUser=id;
    $('.content').remove();
    switch(strUser) {
        case 'HasTitleDoc':
            $('.modal-document').append('<div class="input-group content"><input type="text" id="hasTitle" class="form-control" placeholder="Title" aria-describedby="basic-addon1"></div>');
        break;
        case 'HasYearDoc':
            $('.modal-document').append('<div class="input-group content"><input type="text" id="hasPublicationYear" class="form-control" placeholder="Year" aria-describedby="basic-addon1"></div>');
        break;
        case 'DOIDoc':
            $('.modal-document').append('<div class="input-group content"><input type="text" id="hasDOI" class="form-control" placeholder="DOI" aria-describedby="basic-addon1"></div>');
        break;
        case 'URLDoc':
            $('.modal-document').append('<div class="input-group content"><input type="text" id="hasURL" class="form-control" placeholder="URL" aria-describedby="basic-addon1"></div>');
        break;
        case 'HasAuthorDoc':
            $('.modal-document').append('<div class="input-group content"><input type="text" id="hasAuthor" class="form-control" placeholder="Author" aria-describedby="basic-addon1"></div>');
            //autocomplete autori
            $("#hasAuthor").autocomplete({
            source: autoritotali,
            appendTo: $(".modal-document")
            });
        break;
    }
    if($('.giacomo').length !=0){
        $('.modal-document').append('<p class="content"><input  type="checkbox" id="modaldocuementcheck">'+' Apply on the entire document </p>');
    }else{
        $('.modal-document').append('<h4><span class="label label-warning">Attention , no selection : the annotation will be applied to the entire document</span></h4>');
    }
}

//visualizza le informazioni riguardo le annotazioni effettuate dall utente e le mostra in versione tabellare 
function newRow(){
    $('#annotationTable > tbody').remove();

    for(var i=0; i < TotAnnotation.length;i++){
       //salto un giro se l'ho eliminata e quindi la sua posizione è vuota
        if(TotAnnotation[i]==null){
            continue;
        }

        var valueAnnotation = '';
        var temp = TotAnnotation[i];
        var title = searchTitle(temp.target.source);
        console.log(title);
        var frodo = 'document';
            if(temp.target.start)
               frodo='framment';

        if(temp.annotation[0].type == 'hasAuthor'){
            valueAnnotation = temp.annotation[0].body.resource.label;
        }else{
            valueAnnotation = temp.annotation[0].body.literal;
        }
        $('#annotationTable').append('<tr id='+i+' >' +
            '<td class="imgne"><span class="glyphicon glyphicon-pencil modificaNota" onclick="openModifyModal(this)" data-testo='+i+'></span></td>' +
            '<td class="imgne"><span class="glyphicon glyphicon-remove eliminaNota" data-testo='+i+' onclick="cancelAnnotation(this)"></span></td>' +
            '<td class="type">' + temp.annotation[0].type + '</td>' +
            '<td class="lable">' + temp.annotation[0].label + '</td>' +
            '<td class="titolo">' + title + '</td>' +
            '<td class="tipo">' + frodo + '</td>'+
            '<td class="valueAnnotation valore">'+valueAnnotation+'</td>'
        );

    }
};

//toglie il .print
function UrlFix(Url){
    var res = Url.replace(".print.", ".");
    return res;
}

//si occupa di costruire il json contenente l annotazione sul documento per ora crea un json per ogni annotazione
//TODO: appendere a questo json tutte le annotazioni che si fanno sul documento;
function makeaJsonOnDocument(typeAnnotation,label,document,frammentdocument,valueAnnotation,url) {
    var JsonAnn = {"annotation":[],"target":{},"provenance":{}};
    var temp={};
    var expression=url+"_ver1";
    switch(typeAnnotation){
        case 'hasAuthor':
            idauto=searchAut(valueAnnotation);
            temp = {
                type: typeAnnotation,
                label: "Autore",
                body: {
                    label: "Un autore del documento è " + valueAnnotation,
                    subject: expression,
                    predicate: "http://purl.org/dc/terms/creator",
                    resource: {
                        id: idauto,
                        label: valueAnnotation
                    }
                }
            }
            break;
        case 'hasDOI':
            temp= {
                    type: typeAnnotation ,
                    label: "DOI" ,
                    body: {
                        subject: expression ,
                        predicate: "http://prismstandard.org/namespaces/basic/2.0/do",
                        literal: valueAnnotation
                    }
            }
            break;
        case 'hasPublicationYear':
            temp= {
                type: typeAnnotation ,
                label: "Year" ,
                body: {
                    subject: expression ,
                    predicate: "http://purl.org/spar/fabio/hasPublicationYear",
                    literal: valueAnnotation
                }
            }
            break;
        case 'hasTitle':
            temp= {
                type: typeAnnotation ,
                label: "Title" ,
                body: {
                    subject: expression ,
                    predicate: "http://purl.org/dc/terms/title",
                    literal: valueAnnotation
                }
            }
            break;
        case 'hasURL':
            temp= {
                type: typeAnnotation ,
                label: "Url" ,
                body: {
                    subject: expression ,
                    predicate: "http://purl.org/spar/fabio/hasURL",
                    literal: valueAnnotation
                }
            }
            break;
    }

    JsonAnn.annotation.push(temp);
    if(frammentdocument== true){
        JsonAnn.target = {
            source: url
        };

    }else{
        JsonAnn.target = {
            source: url,
            id: $('.selectedText').getPath(),
            start: Math.min(globalSelectionObj.anchorOffset,globalSelectionObj.focusOffset),
            end: Math.max(globalSelectionObj.anchorOffset,globalSelectionObj.focusOffset)
        };
    }

    JsonAnn.provenance ={
        author: {
            name: $.cookie("username") ,
            email: $.cookie("mail")
        } ,
        time: new Date().getTime()

    }
    TotAnnotation[indexAnn]= JsonAnn;
    indexAnn++;


}

//riconoscimento testo selezionato e inserimento span per colorarlo
function getSelectedText(){
    try {
        if (window.ActiveXObject) {
            var c = document.selection.createRange();
            return c.htmlText;
        }
        globalSelectionObj = getSelection();
        if(getSelection().toString().length > 3){
            //$(document).find('.selectedText');
            $('.selectedText').contents().unwrap();
            $('.giacomo').remove();
            var nNd = document.createElement("span");
            nNd.setAttribute("class","selectedText current");
            var w = getSelection().getRangeAt(0);
            globalSelectionObj = jQuery.extend(true, {}, getSelection());
            w.surroundContents(nNd);
            return nNd.innerHTML;
        }else{
            return null;
        }

    } catch (e) {
        if (window.ActiveXObject) {
            return document.selection.createRange();
        } else {
            return getSelection();
        }
    }
}

//funzione che permette di passare da una formattazione di questo tipo: form_table3_tbody_tr_td_table5_tbody_tr_td_table_tbody_tr_td2_p10_span
//ad una formattata cosi "div >div:eq(3) >div:eq(2) >div:eq(3) >div:eq(5) >div >p >span"
function convert(path){
    var temp = path.split("_");
    var newPath=[];
    var temp2=[];
    var temp3=[];
    var temp4=[];
    var convertedpath='';
    //div5
    for(var i=0 ; i < temp.length; i++){
        temp2 = temp[i].split(/[0-9]/);
        temp3[i]= temp2[0];
        temp2 = temp[i].split(/[a-z]/);
        temp4[i] = temp2[temp2.length -1];
        if (temp4[i]==''){
            newPath[i]=temp3[i];
        }else{
            newPath[i]=temp3[i]+':eq('+temp4[i]+')';
        }
    }

    for(var i = 0; i< newPath.length ; i++) {
        if (i == newPath.length-1) {
            convertedpath += newPath[i]
        } else {
            convertedpath += newPath[i] + ' >';
        }
    }
    return(convertedpath);

}

//apertura documento al click
function setclick(item){
    $(document).on('click',item,function (){
        var titleLocal = $(this).text();
        $('.info-doc').removeClass('nascondi');
        $('.list-doc').addClass('list-doc2');
        loadDocumentFromSever(titleLocal);
    });
}

function loadDocumentFromSever(title){
    $.ajax({
        // definisco il tipo della chiamata
        type: "GET",
        // specifico la URL della risorsa da contattare
        url: "server/server.php",
        // passo dei dati alla risorsa remota
        data: "title="+title,
        // definisco il formato della risposta
        dataType: "json",
        // imposto un'azione per il caso di successo
        success: function(risposta){
            var type = risposta["Type"];
            if(type!= 'Statistica' && type!='AlmaDL Journals'){
                $("#document").html(risposta["Documento"]);
                if ($('#' + 'document' + ' form').children().length === 3) {
                    $('#' + 'document' + ' > form > table:first').addClass("nascondi");
                    $('#' + 'document' + ' > form > table:eq(1)').addClass("nascondi");
                    $('#' + 'document' + ' > form > table:eq(2) > tbody > tr > td > table:eq(4) > tbody > tr > td > table:eq(1)').addClass("nascondi");
                    for (var i = 0; i < 4; i++) {
                        $('#' + 'document' + ' > form > table:eq(2) > tbody > tr > td > table:eq(' + i + ')').addClass("nascondi");
                    }
                }

            }else{
                $("#document").html(risposta["Documento"]);
                $("#fade").addClass("nascondi");
                $("#header").addClass("nascondi");
                $("#sidebar").addClass("nascondi");
                $("#rightSidebar").addClass("nascondi");
                $("#navbar").addClass("nascondi");
                $("#breadcrumb").addClass("nascondi");
                $("#cookiesAlert").addClass("nascondi");
            }

            loadDocumentInfo(title, type);

        },
        // ed una per il caso di fallimento
        error: function(e){
            alert("Chiamata fallita!!!"+ e.responseText);
        }
    })
}

//ricerca autore nell-array e associazione id per annotazione sul documento
function searchAut(value) {
    var autori = value;
    var temp="";
    $.each(JsonAutori,function(i,field){
        if(i== autori){
            temp =field;
        }
    });
    return temp;
}

//chiamata ajax per riempimento autocomplete Autori
function elencoAutori(){
    var temp=[];
    $.ajax({
        type: "GET",
        url: "./server/authorDictionary.json",
        async: false,
        dataType: 'json',
        success: function (risultato) {
            JsonAutori= risultato;
           temp = $.map(risultato, function(el,i) {
               return i });
        }
    });
    //console.log(temp);
    return temp;

}

function openModifyModal(elem){
    var id = elem.getAttribute("data-testo");
    if(TotAnnotation[id].annotation[0].type == 'hasAuthor'){
        var oldVal = TotAnnotation[id].annotation[0].body.resource.label;
        $('#tempAnn').modal('hide');
        $('#myModalModify').modal('show');
        $('#oldAuthor').text("");
        $('#oldAuthor').text("OldAuthor: "+oldVal+"");
        $('#newAuthor').val("");
        $('#newAuthor').autocomplete({
            source: autoritotali,
            appendTo:$(".modal-modify-annotation")
        });
    }else{
        var oldVal = TotAnnotation[id].annotation[0].body.literal;
        $('#tempAnn').modal('hide');
        $('#myModalModify').modal('show');
        $('#oldAuthor').text("");
        $('#oldAuthor').text("OldValue: "+oldVal+"");
        $('#newAuthor').val("");

    }
    indexCorrente=id;

}

function cancelAnnotation(elem){
    var id = elem.getAttribute("data-testo");
    TotAnnotation[id] = null;
    $('#'+ id).remove()

}

function getJsonTitle(){

    $.ajax({
        url: './server/TitleToUrl.json',
        dataType: 'json',
        async: false,
        success: function(result) {

            JsonTitle =  result;
        }
    });
    return JsonTitle;
}

function searchTitle(val){
    var titolo = '';
    $.each(JsonTitle,function(i,field){
        var Url = field['Url'].replace(".print","");
        if(Url == val) {
            titolo=field['Title'];
        }

    });
    return titolo
}